package com.mballem.demoparkapi.web.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UsuarioResDto {

    private Long id;
    private String username;
    private String role;
}
