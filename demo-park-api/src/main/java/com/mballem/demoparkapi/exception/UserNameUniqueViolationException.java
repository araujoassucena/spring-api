package com.mballem.demoparkapi.exception;

public class UserNameUniqueViolationException extends RuntimeException {

    public UserNameUniqueViolationException(String messege){
        super(messege);
    }
}
